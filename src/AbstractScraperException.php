<?php

namespace Abstractmedia\ReactScraper;

/**
 * Class AbstractScraperException
 *
 * @package Abstractmedia\ReactScraper
 */
class ReactScraperException extends \Exception
{
}
