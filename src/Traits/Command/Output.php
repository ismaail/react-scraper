<?php

namespace Abstractmedia\ReactScraper\Traits\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

/**
 * Class Output
 *
 * @package Abstractmedia\ReactScraper\Traits\Command
 */
trait Output
{
    /**
     * @var ConsoleOutput
     */
    private $output;

    /**
     * Initialize output
     */
    private function initOutput()
    {
        if (! $this->output) {
            $this->output = new ConsoleOutput();
        }
    }

    /**
     * @param string|array $message
     * @param string $style
     */
    public function line($message, string $style = null)
    {
        $this->initOutput();

        $styled = $style ? "<$style>$message</$style>" : $message;

        $this->output->writeln($styled);
    }

    /**
     * @param string $message
     */
    public function info($message)
    {
        $this->initOutput();

        $this->line($message, 'info');
    }

    /**
     * @param string $message
     */
    public function warn($message)
    {
        $this->initOutput();

        if (! $this->output->getFormatter()->hasStyle('warning')) {
            $style = new OutputFormatterStyle('yellow');

            $this->output->getFormatter()->setStyle('warning', $style);
        }

        $this->line($message, 'warning');
    }

    /**
     * @param string $message
     */
    public function error($message)
    {
        $this->initOutput();

        $this->line($message, 'error');
    }

    /**
     * @param array $headers
     * @param array $rows
     */
    public function table($headers, $rows)
    {
        $this->initOutput();

        $table = new Table($this->output);
        $table
            ->setHeaders($headers)
            ->setRows($rows)
        ;

        $table->render();
    }
}
