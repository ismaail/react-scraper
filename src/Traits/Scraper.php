<?php

namespace Abstractmedia\ReactScraper\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use React\EventLoop\Factory;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Handler\CurlMultiHandler;

/**
 * @deprecated
 */
trait Scraper
{
    public $client		    = null;
    public $items		    = null;

    /**
     * The jobs pool
     *
     * @var Collection
     */
    public $pool			= null;

    /**
     * List of the processed jobs
     *
     * @var array
     */
    public $processed		= [];

    /**
     * Max jobs to perform in the same time
     *
     * @var int
     */
    public $maxQueue     	= 30;


    /**
     * Setup and Start the Event Loop
     *
     * @param array $params
     */
    public function setup ($params = []) {

        $this->pool = collect();

        // Create a React event loop
        $loop = Factory::create();
        $scraper = $this;

        // Create a Guzzle handler that integrates with React
        $handler = new CurlMultiHandler();
        $timer = $loop->addPeriodicTimer(0, \Closure::bind(function () use (&$timer, $scraper) {
            $this->tick();
            $count      = count($this->handles);

            if (!$scraper->pool->isEmpty()) {
                $scraper->line('STILL IN QUEUE : ' . $scraper->pool->count());
            }

            if ($count < $scraper->maxQueue && !$scraper->pool->isEmpty()) {
                $scraper->handleJobs($scraper->maxQueue - $count);
            }

            if (empty($this->handles) && Promise\queue()->isEmpty()) {
                $scraper->info('TIMER CANCELING ...');
                $timer->cancel();
            }
        }, $handler, $handler));

        $this->pool	= collect([]);

        // Create a Guzzle client that uses the special handler
        $config             = $params;
        $config['handler']  = HandlerStack::create($handler);
        if (!isset($config['headers']['User-Agent'])) {
            $config['headers']  = [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36',
            ];
        }
        $this->client = new Client($config);

        $this->init();

        // Run the queue
        $loop->run();
    }


    public function handleJobs ($number = 1) {

        if($this->pool->count()) {

            for($i = 0; $i < $number && !$this->pool->isEmpty(); $i++) {
                $job   		= $this->pool->pop();

                if ($job['retries'] >= 3) {
                    continue;
                }

                if (isset($this->tasks[$job['type']])) {
                    $this->{$this->tasks[$job['type']]}($job);
                } else {
                    $this->error($job['type'] . ' Not implemented');
                }
            }
        }

    }

    public function request($job, $callback, $headers = [], $method = 'GET') {

        $this->warn('GET : ' . $job['url']);
        $this->processed[$job['url']]  = false;
        $async  = strtolower($method) . 'Async';

        if (isset($job['data']['headers'])) {
            $headers = $job['data']['headers'];
        }

        $this->client->{$async}($job['url'], $headers)->then(
            function (ResponseInterface $response) use($job, $callback) {
                $this->info('GOT : ' . $job['url']);
                $this->processed[$job['hash']]  = true;

                $callback($response->getBody()->getContents());
            }, function (\Exception $error) use($job) {
                // Called if request failed
                unset($this->processed[$job['url']]);
                $job['retries']++;
                $this->job($job);
                $this->warn('Error: ' . $error->getMessage() . "\n");
            }
        );

    }

    public function post($job, $callback, $headers = []) {
        $this->request($job, $callback, $headers, 'POST');
    }

    public function get($job, $callback, $headers = []) {
        $this->request($job, $callback, $headers);
    }

    public function job($job) {
        if (!isset($job['data'])) {
            $job['data']    = [];
        }

        if (!isset($job['hash'])) {
            $job['hash'] = md5(json_encode($job));
        }

        if (!isset($job['retries'])) {
            $job['retries'] = 0;
        }

        $founded    = $this->pool->search(function ($item, $key) use($job) {
            return $item['hash'] == $job['hash'];
        });

        if ($founded === false && !isset($this->processed[$job['hash']])) {
            $this->pool->push($job);

            return true;
        }

        return false;
    }

    public function unverifiedJob($job) {
        if (!isset($job['data'])) {
            $job['data']    = [];
        }
        if (!isset($job['retries'])) {
            $job['retries'] = 0;
        }
        $this->pool->push($job);
    }

    private function getProxy() {
        $proxies    = collect([]);

        return $proxies->random();
    }
}