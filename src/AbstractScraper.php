<?php

namespace Abstractmedia\ReactScraper;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use React\EventLoop\Factory;
use GuzzleHttp\HandlerStack;
use React\EventLoop\Timer\Timer;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Handler\CurlMultiHandler;

/**
 * Class AbstractScraper
 *
 * @package App\ReactScraper
 *
 * @property array $handles
 * @mixin \GuzzleHttp\Handler\CurlMultiHandler
 */
abstract class AbstractScraper
{
    /**
     * @var \GuzzleHttp\Client
     */
    public $client;

    /**
     * The jobs pool
     *
     * @var Collection
     */
    public $pool;

    /**
     * Max jobs to perform in the same time.
     *
     * @var int
     */
    public $maxQueue = 30;

    /**
     * @var int
     */
    protected $maxTries = 3;

    /**
     * List of the processed jobs.
     *
     * @var array
     */
    public $processed = [];

    /**
     * @param array $clientConfig
     * @param bool $continueLoop
     */
    protected function setup(array $clientConfig = [], bool $continueLoop = false)
    {
        $this->pool = collect();

        $source = $this;

        $callback = function (Timer $timer) use ($source, $continueLoop) {
            $this->tick();
            $count = \count($this->handles);

            if ($count < $source->maxQueue && !$source->pool->isEmpty()) {
                $source->handleJobs($source->maxQueue - $count);
            }

            if (!$continueLoop && empty($this->handles) && Promise\queue()->isEmpty()) {
                $timer->cancel();
            }
        };

        $loop = Factory::create();
        $handler = new CurlMultiHandler();
        $loop->addPeriodicTimer(0, $callback->bindTo($handler, CurlMultiHandler::class));

        $this->pool = collect([]);

        // Create a Guzzle client that uses the special handler
        $clientConfig['handler'] = HandlerStack::create($handler);

        $this->client = new Client($clientConfig);

        $this->initJobs();

        // Run the queue
        $loop->run();
    }

    /**
     * @param int $number
     */
    public function handleJobs(int $number = 1): void
    {
        if (! $this->pool->count()) {
            return;
        }

        for ($i = 0; $i < $number && !$this->pool->isEmpty(); $i++) {
            $job = $this->pool->shift();

            if ($job['retries'] >= $this->maxTries) {
                continue;
            }

            // Convert job type to StudlyCaps and prefix with 'job'
            $taskName = 'job' . str_replace(' ', '', ucwords(str_replace('_', ' ', $job['type'])));

            if (! method_exists(\get_class($this), $taskName)) {
                throw new ReactScraperException(sprintf("Job Method '{$taskName}' not implemented"));
            }

            $this->{$taskName}($job);
        }
    }

    /**
     * Push new Job
     *
     * @param array $job
     *
     * @return bool
     */
    public function pushJob(array $job): bool
    {
        $job['data'] = $job['data'] ?? [];
        $job['hash'] = $job['hash'] ?? md5(json_encode($job));
        $job['retries'] = $job['retries'] ?? 0;

        $found = $this->pool->search(function ($item) use ($job) {
            return $item['hash'] === $job['hash'];
        });

        if (false === $found && !isset($this->processed[$job['hash']])) {
            $this->pool->push($job);

            return true;
        }

        return false;
    }

    /**
     * Send Http GET request
     *
     * @param array $job
     * @param callable $callback
     * @param array $headers
     */
    public function getRequest(array $job, callable $callback, array $headers = []): void
    {
        $this->request($job, $callback, $headers);
    }

    /**
     * Send Http POST request
     *
     * @param array $job
     * @param callable $callback
     * @param array $options
     */
    public function postRequest(array $job, callable $callback, array $options = []): void
    {
        $this->request($job, $callback, $options, 'POST');
    }

    /**
     * @param array $job
     * @param callable $callback
     * @param array $options
     * @param string $method
     */
    public function request(array $job, callable $callback, array $options = [], string $method = 'GET'): void
    {
        $this->processed[$job['url']]  = false;
        $async  = strtolower($method) . 'Async';

        $this->client->{$async}($job['url'], $options)->then(
            function (ResponseInterface $response) use ($job, $callback, $method) {
                $this->processed[$job['hash']] = true;

                $callback($response->getBody()->getContents());
            },
            function (\Exception $exception) use ($job) {
                // Called if request failed
                unset($this->processed[$job['url']]);
                $job['retries']++;
                $this->pushJob($job);
                /** @todo If max tries is hit, throw Exception */
            }
        );
    }

    /**
     * Start the loop process.
     */
    abstract public function run(): void;

    /**
     * Initialize Jobs
     */
    abstract public function initJobs(): void;
}
